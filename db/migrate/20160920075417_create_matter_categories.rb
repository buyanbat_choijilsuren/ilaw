class CreateMatterCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :matter_categories do |t|

    	t.string :code
    	t.string :name

    	t.integer :status, default: 0
    	t.text :note

      t.timestamps
    end
  end
end
