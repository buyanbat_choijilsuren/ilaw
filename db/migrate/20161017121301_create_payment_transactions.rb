class CreatePaymentTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :payment_transactions do |t|

    	t.string :code
    	t.integer :payment_id
    	t.integer :customer_id
    	t.integer :matter_id
    	t.string :currency
    	t.decimal :amount
    	t.date	:trn_date
    	t.string :title
    	t.string :description

    	t.integer :status, default: 0
      t.timestamps
    end
  end
end
