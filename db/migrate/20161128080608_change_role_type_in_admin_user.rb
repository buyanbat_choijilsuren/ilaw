class ChangeRoleTypeInAdminUser < ActiveRecord::Migration[5.0]
  def change
  	add_column :admin_users, :role_id, :integer
  	remove_column :admin_users, :role
  end
end
