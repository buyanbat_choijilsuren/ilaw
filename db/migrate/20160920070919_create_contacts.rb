class CreateContacts < ActiveRecord::Migration[5.0]
  def change
    create_table :contacts do |t|

    	t.integer :customer_id
    	t.integer :contact_type
    	
    	t.string :address_line
    	t.string :address_line1
    	t.string :address_line2
    	t.string :phone

      t.timestamps
    end
  end
end
