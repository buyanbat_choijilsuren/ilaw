class AddProgressToEvents < ActiveRecord::Migration[5.0]
  def change
  	add_column :events, :progress, :integer, default: 0
  end
end
