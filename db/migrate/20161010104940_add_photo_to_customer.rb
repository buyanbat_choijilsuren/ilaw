class AddPhotoToCustomer < ActiveRecord::Migration[5.0]
  def self.up
  	add_attachment :customers, :photo
  end

  def self.down
  	remove_attachment :customers, :photo
  end
end
