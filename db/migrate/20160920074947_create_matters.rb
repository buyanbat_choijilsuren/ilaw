class CreateMatters < ActiveRecord::Migration[5.0]
  def change
    create_table :matters do |t|
    	t.string :code
    	t.integer :customer_id
    	t.string :title
    	t.date :start_date
    	t.date :due_date
    	t.integer :category_id

      t.decimal :amount
      t.string  :currency
      t.integer :payment_method
      t.boolean :vats
      t.text :note

    	t.integer :status, default: 0

      t.timestamps
    end
  end
end
