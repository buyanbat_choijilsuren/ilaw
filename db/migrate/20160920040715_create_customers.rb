class CreateCustomers < ActiveRecord::Migration[5.0]
  def change
    create_table :customers do |t|

    	t.string :code
    	t.string :name
    	t.string :title
    	t.integer :customer_type, default: 0

    	t.string :job
        t.string :email
        t.string :phone

    	t.date :birth_date
    	t.string :register_no
    	t.integer :gender
    	t.text :note
    	t.integer :status


      t.timestamps
    end
  end
end
