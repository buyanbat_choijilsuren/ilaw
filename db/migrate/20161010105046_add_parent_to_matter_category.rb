class AddParentToMatterCategory < ActiveRecord::Migration[5.0]
  def change
  	add_column :matter_categories, :parent_id, :integer
  end
end
