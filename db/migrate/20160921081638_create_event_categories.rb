class CreateEventCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :event_categories do |t|

    	t.string :name
    	t.string :code
    	t.integer :status, default: 0
    	t.string :details

      t.timestamps
    end
  end
end
