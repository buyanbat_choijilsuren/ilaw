class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|

    	t.integer :matter_id
    	t.integer :customer_id
    	t.integer :category_id
    	t.datetime :start_date
    	t.datetime :due_date
    	t.integer :status, default: 0

    	t.string :title
    	t.text :note

      t.timestamps
    end
  end
end
