class CreateDocuments < ActiveRecord::Migration[5.0]
  def change
    create_table :documents do |t|

    	t.integer :matter_id
    	t.string :title

    	t.attachment :file

    	t.integer :status, :default => 0

      t.timestamps
    end
  end
end
