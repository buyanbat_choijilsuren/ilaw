class AddPhotoToUser < ActiveRecord::Migration[5.0]
  def self.up
  	add_attachment :admin_users, :photo
  end

  def self.down
  	remove_attachment :admin_users, :photo
  end
end
