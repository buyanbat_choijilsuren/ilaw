class CreatePayments < ActiveRecord::Migration[5.0]
  def change
    create_table :payments do |t|

    	t.string :code
    	t.string :title

    	t.integer :customer_id
    	t.integer :matter_id
    	t.date :due_date
    	t.decimal :amount
    	t.decimal :amount_paid, default: 0
    	t.string :currency
    	t.string :ref_code
    	t.string :details

    	t.integer :status, default: 0

      t.timestamps
    end
  end
end
