class CreateReminders < ActiveRecord::Migration[5.0]
  def change
    create_table :reminders do |t|
    	t.string :code
    	t.integer :user_id
    	t.integer :status
    	t.integer :reminder_category
    	t.string :title
    	t.text :body
    	t.datetime :date
    	t.datetime :seen_date

    	t.string :resource_type
    	t.integer :resource_id

      t.timestamps
    end
  end
end
