# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password', name: "Buyanbat.Ch" , job: "System Engineer", phone: "99033672", role: "admin")

crime = MatterCategory.create!(code: "01", name: " хэрэг")
personal = MatterCategory.create!(code: "02", name: "Иргэний хэрэг")
zahirgaa = MatterCategory.create!(code: "03", name: "Захиргааны хэрэг")
other = MatterCategory.create!(code: "04", name: "Бусад")


EventCategory.create!(code: "01", name: "Уулзалт")
EventCategory.create!(code: "02", name: "Хурал")
EventCategory.create!(code: "03", name: "Сургалт")



