class Document < ApplicationRecord
	
	include PublicActivity::Model
	tracked owner: Proc.new{ |controller, model| controller.current_admin_user }

	DOCUMENT_TYPES = ["application/msword", "application/pdf", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
										"application/vnd.ms-excel", "application/vnd.ms-powerpoint", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"]
	IMAGE_TYPES = ['image/jpeg', 'image/pjpeg', 'image/gif', 'image/png', 'image/x-png', 'image/jpg']
	MEDIA_TYPES = ["video/mpeg","video/mp4","application/mp4"]


	acts_as_taggable_on :tags
	
	belongs_to :matter

	has_attached_file :file, styles: -> (attachment) { attachment.instance.is_image? ? { thumb: "200x110#" } : {} },
  	default_url:  ->(attachment) { ActionController::Base.helpers.asset_path("photo-missing-mediums.png") }


	scope :with_customer, -> (ids) { joins(:matter).where(matters: {customer_id: ids}) }
	scope :with_matter, -> (ids) { where(matter: ids) }
	scope :with_name, -> (name) {where("file_file_name like ?","%#{name}%")}
	scope :with_content, -> (contents) { where(file_content_type:  contents)}

	validates_attachment :file, presence: true,
	  content_type: { content_type: [DOCUMENT_TYPES + IMAGE_TYPES + MEDIA_TYPES] },
	  size: { in: 0..100.megabytes }

	validates :matter, presence: true
	validates :title, presence: true

	def is_image?
		return false unless self.file_content_type
		IMAGE_TYPES.include? self.file_content_type
	end
end