class Event < ApplicationRecord

	include PublicActivity::Model
	tracked owner: Proc.new{ |controller, model| controller.current_admin_user }

	belongs_to :customer
	belongs_to :matter
	belongs_to :category, class_name: "EventCategory", foreign_key: :category_id

	has_many :assignments, as: :duty

	has_many :users, through: :assignments

	enum status: [:open, :completed, :canceled]
	validates :category, presence: true
	validates :title, presence: true
	validates :start_date, presence: true
	validates :due_date, presence: true
	validates :users, presence: true


	def status_class 
		if self.open?
			:success
		elsif self.completed?
			:primary
		else
			:default
		end
	end
	
end
