class Role < ApplicationRecord
	has_many :permissions
	has_many :users, class_name: "AdminUser", foreign_key: :role_id
end
