class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    user ||= AdminUser.new # guest user (not logged in)
    if user.role.name == "Admin"
      can :manage, :all
    else
      can :update, AdminUser, :id => user.id
      can :read, AdminUser, :id => user.id

      user.role.permissions.each do |permission|
        if permission.subject_class == "dashboard" 
          can :read, ActiveAdmin::Page, name: "Dashboard" unless permission.action == "denied"
        elsif  permission.subject_class == "calendar"
          can :read, ActiveAdmin::Page, name: "Calendar" unless permission.action == "denied"
        else
          if permission.action == "read"
            can permission.action.to_sym, permission.subject_class.classify.constantize
          elsif permission.action == "update"
            can :read, permission.subject_class.classify.constantize
            can permission.action.to_sym, permission.subject_class.classify.constantize
          elsif permission.action == "create"
            can :read, permission.subject_class.classify.constantize
            can :update, permission.subject_class.classify.constantize
            can permission.action.to_sym, permission.subject_class.classify.constantize
          elsif permission.action == "manage"
            can permission.action.to_sym, permission.subject_class.classify.constantize
          end
        end
      end
    end


    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
