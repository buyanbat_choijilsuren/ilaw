class AdminUser < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  belongs_to :role

  devise :database_authenticatable, 
         :recoverable, :rememberable, :trackable, :validatable

  has_attached_file :photo, styles: { medium: "530x350#", small: "350x150>", thumb: "100x100>" }, 
  	default_url: ->(attachment) {ActionController::Base.helpers.asset_path("photo-missing-mediums.png")}

  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\z/

end
