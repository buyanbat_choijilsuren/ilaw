class Assignment < ApplicationRecord
	belongs_to :duty, polymorphic: true

	belongs_to :user, class_name: "AdminUser", foreign_key: :user_id
end
