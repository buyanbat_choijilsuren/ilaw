class Matter < ApplicationRecord

	include PublicActivity::Model
	tracked owner: Proc.new{ |controller, model| controller.current_admin_user }

	
	belongs_to :customer
	belongs_to :category, class_name: "MatterCategory", foreign_key: :category_id

	has_many :payments
	has_many :documents
	has_many :events

	enum payment_method: [:constant, :scheduled]

	enum status: [:open, :pending, :solved, :canceled]

	after_create :set_code

	accepts_nested_attributes_for :documents, :allow_destroy => true

	validates :customer, presence: true
	validates :category, presence: true
	validates :title, presence: true

	attr_accessor :name

	def name
		self.code
	end


	def status_class
		if self.open?
			:success
		elsif self.pending?
			:warning
		elsif self.solved?
			:primary
		else
			:danger
		end
	end
	
	protected
		def set_code
			self.code = "#{self.category.code}-#{self.id.to_s.rjust(6,'0')}"
			self.save
		end
end
