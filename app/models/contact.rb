class Contact < ApplicationRecord
	belongs_to :customer

	enum contact_type: [:home, :work, :other]
end
