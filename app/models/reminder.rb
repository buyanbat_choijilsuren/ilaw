class Reminder < ApplicationRecord
  belongs_to :resource, polymorphic: true
  belongs_to :user, class_name: "AdminUser", foreign_key: :user_id

  enum status: [:open, :seen]

  def self.cron_events
  	events = Event.open.where("start_date <= ?", Time.now + 1.hrs)

  	events.each do |event|
  		diff = Time.now - event.start_date
  		event.users.each do |user|
				reminder = Reminder.where(resource: event, user: user)

	  		if diff > 30.minutes
	  			unless reminder.size > 0
	  				Reminder.create(resource: event, user: user, date: event.start_date, title: event.category.name, body: event.title, code: "01", status: 0)
	  			end

	  		elsif diff > 5.minutes
	  			if reminder.size > 0 
	  				unless reminder.first.code == "02"
	  					reminder.first.date = event.start_date
	  					reminder.first.code = "02"
	  					reminder.first.open!
	  				end
	  			else
	  				Reminder.create(resource: event, user: user, date: event.start_date, title: event.category.name, body: event.title, code: "02", status: 0)
	  			end
	  		else
  				if reminder.size > 0 
	  				unless reminder.first.code == "03"
	  					reminder.first.date = event.start_date
	  					reminder.first.code = "03"
	  					reminder.first.open!
	  				end
	  			else
	  				Reminder.create(resource: event, user: user, date: event.start_date, title: event.category.name, body: event.title, code: "03", status: 0)
	  			end
	  		end
	  	end
  	end
  end
end
