class MatterCategory < ApplicationRecord
	enum status: [:enabled, :disabled]

	has_many :matters, foreign_key: :category_id

	belongs_to :parent, :class_name => 'MatterCategory', :foreign_key => :parent_id
	has_many :childs, :class_name => 'MatterCategory', :foreign_key => :parent_id

	after_create :set_code

	def set_code
		if self.parent
			self.code = "#{self.parent.code}#{self.id.to_s.rjust(3,'0')}"
			self.save
		end
	end
end
