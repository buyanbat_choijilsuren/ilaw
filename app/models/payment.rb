class Payment < ApplicationRecord

	include PublicActivity::Model
	tracked owner: Proc.new{ |controller, model| controller.current_admin_user }
	
	belongs_to :customer
	belongs_to :matter
	has_many :transactions, class_name: "PaymentTransaction", foreign_key: :payment_id

	enum status: [:open, :paid, :canceled]

	after_create :set_code

	validates :customer, presence: true
	validates :matter, presence: true
	validates :title, presence: true


	def self.currencies 
		['MNT', 'USD', 'EUR']
	end

	def set_code
		self.code = "INV-#{self.id.to_s.rjust(6,'0')}"
		self.save
	end

	def balance
		if self.amount 
			self.amount - self.transactions.authorized.sum(:amount)  
		else
			00
		end
	end

	def status_class
		if self.open?
			:success
		elsif self.paid?
			:primary
		else
			:default
		end
	end
end
