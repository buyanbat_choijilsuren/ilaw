class Permission < ApplicationRecord
	belongs_to :role

	CONST_CLASSES = [
		['admin_user', 'Хэрэглэгч'],
		['customer', 'Харилцагч'],
		['matter','Гэрээ'],
		['event','Ажил төлөвлөлт'],
		['event_category','Ажил төлөвлөлтийн ангилал'],
		['payment','Төлбөр'],
		['payment_transaction','Төлбөрийн гүйлгээ'],
		['document','Хавсралт'],
		['matter_category','Гэрээний ангилал'],
		['dashboard', 'Үндсэн самбар'],
		['calendar', "Календарь"]
	]
	CONST_ACTIONS = [
		['denied','Denied'],
		['read', 'View'],
		['update', 'Update'],
		['create', 'Create'],
		['manage', "Manage"]
	]
end
