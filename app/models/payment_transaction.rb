class PaymentTransaction < ApplicationRecord
	belongs_to :payment
	belongs_to :customer
	belongs_to :matter

	enum status: [:open, :authorized, :canceled]
	
	include PublicActivity::Model
	tracked owner: Proc.new{ |controller, model| controller.current_admin_user }

	after_create :set_matter

	def set_matter
		self.code = "#{self.payment.code}-#{self.id.to_s}"
		self.customer_id = self.payment.customer_id 
		self.matter_id = self.payment.matter_id
		self.save
	end

	def authorize
		self.authorized!
		self.payment.paid! if self.payment.balance <= 0 
	end

	def status_class 
		if self.open?
			:success
		elsif self.authorized?
			:primary
		else
			:danger
		end
	end
end
