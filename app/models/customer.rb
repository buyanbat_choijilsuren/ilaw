class Customer < ApplicationRecord

	include PublicActivity::Model
	tracked owner: Proc.new{ |controller, model| controller.current_admin_user }

  has_attached_file :photo, styles: { medium: "530x350#", small: "350x150>", thumb: "100x100>" }, 
  	default_url:  -> (attachment) {ActionController::Base.helpers.asset_path("photo-missing-mediums.png")}

	enum customer_type: [:personal, :company]
	enum gender: [:male, :female]
	enum status: [:active, :inactive]

	has_many :contacts
	has_many :matters
	has_many :documents, through: :matters
	has_many :payments
	has_many :events

	accepts_nested_attributes_for :contacts, :allow_destroy => true

	after_create :set_code

	validates :customer_type, presence: true
	validates :title, presence: true, if: Proc.new {|a| a.personal? }
	validates :name, presence: true
	validates :register_no, presence: true, length: {minimum: 7}
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\z/


	def set_code
		self.code = self.id.to_s.rjust(6,"0")
		self.active!
	end
	def status_class 
		 self.active? ? :primary : :default
	end

	def self.search text
		if text
			where("name like ? ","%#{text}%")
		else
			scoped
		end
	end

	def search_title
		"#{self.code} | #{self.name}"
	end

	def search_meta
		"#{self.customer_type} | #{self.title} #{self.name}(#{self.register_no}) | #{self.created_at} | #{self.email} | #{self.phone}"
	end

	def search_text
		self.note.html_safe
	end

end
