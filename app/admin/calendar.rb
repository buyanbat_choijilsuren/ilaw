ActiveAdmin.register_page "Calendar" do

	menu icon: "fa fa-calendar", priority: 5, label: "Календарь"

	content title: "Календарь"do 
		div class: "col-lg-9" do
			div class: "ibox" do 
				div class: "ibox-content" do 
					div id: "calendar", class: :full_calendar do 
					end
				end
			end
		end


		div class: "col-lg-3" do
			a href: new_admin_event_path, class: "btn btn-primary btn-sm btn-block", style: "text-align: left;" do 
				i class: "fa fa-tasks m-r"
				text_node "Ажил төлөвлөлт бүртгэх"
			end
			a href: new_admin_payment_path, class: "btn btn-danger btn-sm btn-block", style: "text-align: left;" do 
				i class: "fa fa-credit-card m-r"
				text_node "Төлбөр бүртгэх"
			end
			a href: new_admin_event_path(type: :task), class: "btn btn-warning btn-sm btn-block", style: "text-align: left;" do 
				i class: "fa fa-plus-square m-r"
				text_node "Хувийн ажил төлөвлөх"
			end
			panel "Хэрэглэгчид", class: "m-t" do
				users = AdminUser.all
				table_for users, class: "table table-hover issue-tracker" do
					column "Сонгох" do |res|
						if res.id == current_admin_user.id
							raw "<input type='checkbox' name='user[#{res.id}]' class='i-checks' id='user_checker_#{res.id}' checked=true />"
						else
							raw "<input type='checkbox' name='user[#{res.id}]' class='i-checks' id='user_checker_#{res.id}' />"
						end
					end
					column "", class: "project-people" do |obj_user|
			      img src: obj_user.photo.url(:small), class: "img-circle"
			    end
					column "Хэрэглэгч", :name
				end
				div class: "ibox-footer" do
					a class: "btn btn-info btn-block btn-rounded calendar-updater" do 
						i class: "fa fa-refresh m-r"
						text_node "Шинэчилэх"
					end
				end
			end
		
		end

		div class: "modal inmodal" , id: "calendar-modal" do 
			div class: "modal-dialog" do 
				div class:"modal-content animated bounceInRight" do 
					div class: "modal-header" do 
					end
					div class: "modal-body" do 
					end
					div class: "modal-footer" do 
					end
				end
			end
		end
	end

	page_action :search, method: :get do 
		@customers = Customer.search(params[:search])
	end

	page_action :events, method: :get do 
		@events = Event.where.not(matter: nil)
		res = []
		@events.each do |event|
			data = {
				id: "events?id=#{event.id}",
				title: "#{event.title} - #{event.matter.code})",
				start: event.start_date,
				end: event.due_date,
				url: admin_event_path(event),
				users: event.users.map {|u| u.id }
			} 
			res << data
		end
		respond_to do |format|
			format.json {render json: res }
		end
	end

	page_action :payments, method: :get do 
		@payments = Payment.all
		res = []
		@payments.each do |payment|
			data = {
				id: "payments?id=#{payment.id}",
				title: "#{payment.code} - #{payment.title} ",
				start: payment.due_date,
				url: admin_payment_path(payment),
				users: ["all"]
			} 
			res << data
		end
		respond_to do |format|
			format.json {render json: res }
		end
	end

	page_action :tasks, method: :get do 
		@events = Event.where(matter: nil)
		res = []
		@events.each do |event|
			data = {
				id: "events?id=#{event.id}",
				title: "#{event.title}",
				start: event.due_date,
				url: admin_event_path(event),
				users: event.users.map {|u| u.id }
			} 
			res << data
		end
		respond_to do |format|
			format.json {render json: res }
		end
	end

	page_action :update_events, method: :get do 
		event = Event.find(params[:id])
		if event.open?
			event.start_date = params[:start]
			event.due_date = params[:end]
			event.save
		end
	end

	page_action :update_payments, method: :get do 
		payment = Payment.find(params[:id])
		if payment.open?
			payment.due_date = params[:start]
			payment.save
		end
	end

end