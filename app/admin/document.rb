ActiveAdmin.register Document do 

	config.per_page = 2

	filter :matter
	filter :title
	filter :file_file_name
	filter :created_at
	
	menu priority: 7, label: "Хавсралт", icon: "fa fa-paperclip"
	index as: :block do |doc|
		div class: "file-box" do 

			div class: :file do 
	      a  href: admin_matter_path(doc.matter), class: "text-center border-bottom btn btn-block btn-sm" do 
	      	i class: "fa fa-tags"
	      	text_node doc.matter.code
	      end
				a href: download_admin_document_path(doc) do 
					span class: "corner"
					if ["image/jpeg", "image/png", "image/gif"].include? doc.file_content_type
						div class: :image do 
							img src: doc.file.url(:thumb) , class: "img-responsive"
						end
					else
						div class: :icon do 
							i class: "fa fa-file"
						end
					end
					div class: "file-name" do 
						text_node doc.file_file_name.truncate 25
						br 
						small(doc.title)
					end 
				end
			end
		end
	end

	form title: "Хавсралт файл" do |f|
		f.object.matter_id = params[:matter_id] if params[:matter_id]
		div class: :row  do 
			div class: "col-sm-6" do 
				f.inputs do 
					if params[:customer_id]
						f.input :matter, input_html: {class: "form-control chosen-select"}, label: "Гэрээ", 
							collection: Matter.where(customer_id: params[:customer_id]).collect {|m| [m.code, m.id]}
					else
						f.input :matter, input_html: {class: "form-control chosen-select"}, label: "Гэрээ"
					end
					f.input :title, label: "Хавсралтын тайлбар"
					f.input :tag_list, input_html: {class: "form-control select2", multiple: :multiple}, collection: ActsAsTaggableOn::Tag.all.pluck(:name), label: "Түлхүүр үгс"
					f.input :file, label: "Хавсралт файл"
				end
				f.actions
			end
		end
	end

	member_action :download, method: :get do
	  @document = Document.find(params[:id])
		send_file @document.file.path, file_name: @document.file_file_name, type: @document.file_content_type
	end

	collection_action :list, method: :get do 
		@page_title = "Хавсралтууд"
		@list = Document.order(created_at: :desc).take(20)
	end

	action_item :new_documents, only: :list do 
		a href: new_admin_document_path, class: "btn btn-primary" do 
			i class: "fa fa-plus m-r"
			text_node "Хавсралт нэмэх"
		end
	end
	collection_action :search , method: :get do
		@list = Document.all
		@list = @list.with_customer(params[:customers]) if params[:customers] and params[:customers].size > 0
		@list = @list.with_matter(params[:matters]) if params[:matters] and params[:matters].size > 0 
		@list = @list.with_name(params[:file_name]) if params[:file_name] and params[:file_name].size > 0
		@list = @list.tagged_with(params[:tags], any: true) if params[:tags] and params[:tags].size > 0

		if params[:contents] and params[:contents].size > 0 and params[:contents] != "all"
			board = {
				docs: Document::DOCUMENT_TYPES,
				images: Document::IMAGE_TYPES,
				media: Document::MEDIA_TYPES
			}
			@list = @list.with_content(board[params[:contents].to_sym])
		end
		@list = @list.order(created_at: :desc).take(20)

		render layout: false
	end
	controller do 
    def permitted_params
      params.permit!
      # params.permit! # allow all parameters
    end
    def create(options={}, &block)
      tags = params[:document].delete :tag_list
      tags.shift

      document = build_resource
      document.tag_list = tags.join(',')

      if create_resource(document)
        options[:location] ||= smart_resource_url
      end
      respond_with_dual_blocks(document, options, &block)
    end
		def update			
	    tags = params[:document].delete :tag_list
	    tags.shift
	 
			update!

	    document = build_resource
	    document.tag_list = tags.join(',')
	    document.save
   	end

   	def index
   		redirect_to list_admin_documents_path
   	end
	end
end