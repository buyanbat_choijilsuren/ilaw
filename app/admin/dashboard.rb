ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, icon: "fa fa-th-large", label: "Үндсэн самбар"

  content title: proc{ I18n.t("active_admin.dashboard") } do
    div class: :row do
      div class: "col-md-4 col-sm-6" do 
        div class: "ibox" do 
          div class: "ibox-title" do 
            a href: new_admin_customer_path, class: "btn btn-sm btn-primary pull-right" do 
              i class: "fa fa-group m-r-sm"
              text_node "Харилцагч бүртгэх"
            end
            h4 "Харилцагч"
          end
          div class: "ibox-content" do 
            h1 Customer.count, class: "no-margins"
            small "Идэвхитэй" 
            div class: "stat-percent font-bold text-success" do 
              if Customer.count > 0
                text_node (Customer.active.count  * 100 / Customer.count).to_s + "%" 
              end
            end
          end
        end
      end
      div class: "col-md-4 col-sm-6" do 
        div class: "ibox" do 
          div class: "ibox-title" do  
            a href: new_admin_matter_path, class: "btn btn-sm btn-primary pull-right" do 
              i class: "fa fa-briefcase m-r-sm"
              text_node "Гэрээ бүртгэх"
            end
            h4 "Гэрээ"
          end
          div class: "ibox-content" do 
            h1 Matter.count, class: "no-margins"
            small "Идэвхитэй" 
            div class: "stat-percent font-bold text-success" do 
              span Matter.open.count 
            end
          end
        end
      end

      div class: "col-md-4" do 
        div class: "ibox" do 
          div class: "ibox-title" do
            a href: new_admin_payment_path, class: "btn btn-sm btn-primary pull-right" do 
              i class: "fa fa-dollar m-r-sm"
              text_node "Төлбөр бүртгэх"
            end
            h4 "Төлбөр"
          end
          div class: "ibox-content" do 
            h1 number_with_delimiter(Payment.all.sum(:amount)), class: "no-margins"
            small "Үлдэгдэл" 
            div class: "stat-percent font-bold text-danger" do 
              span number_with_delimiter Payment.open.sum(:amount) 
            end
          end
        end
      end
    end
    div class: :row do 
      div class: "col-md-6" do 
        panel "Шинэ гэрээ, харилцагчийн график" do 
          div do
            canvas id: "barChart",  height: "100"
          end
          6.times do |i|
            then_date = Time.now - (5 - i).month
            cust_count = Customer.where("date_part('month', created_at) = ?", then_date.month).count
            matter_count = Matter.where("date_part('month', created_at) = ?", then_date.month).count
            span "data-customer": cust_count, "data-matter": matter_count, "data-label": then_date.strftime("%b %Y"), class: "bar-chart-customer"
          end
        end
      end
      div class: "col-md-6" do 
        panel "Нийт гэрээ, харилцагчийн график" do 
          div do
            canvas id: "lineChart",  height: "100"
          end
          6.times do |i|
            then_date = Time.now - (4 - i).month
            cust_count = Customer.where("created_at < ?", then_date.strftime("%Y-%m-01")).count
            matter_count = Matter.where("created_at < ?", then_date.strftime("%Y-%m-01")).count
            span "data-customer": cust_count, "data-matter": matter_count, "data-label": (then_date-1.month).strftime("%b %Y"), class: "line-chart-customer"
          end
        end
      end
    end
    div class: :row do 
      div class: "col-md-6" do 
        div class: "ibox" do 
          div class: "ibox-title" do 
            a href: admin_calendar_path, class: "btn btn-sm btn-white pull-right" do 
              i class: "fa fa-calendar m-r-sm"
              text_node "Дэлгэрэнгүй"
            end
            h4 "Ажлын жагсаалт"
          end
          div class: "ibox-content" do 
            Event.open.order(start_date: :asc).take(5).each do |event| 
              div class: "timeline-item" do 
                div class: :row do 
                  div class: "col-xs-3 date" do 
                    i class: "fa fa-clock-o"
                    text_node event.start_date.strftime("%b %d %H:%M")
                    br
                    small time_diff(event.start_date, Time.now), class: event.start_date > Time.now ?  "text-navy" : "text-danger"
                  end
                  div class: "col-xs-9 content project-title" do 
                    a href: admin_event_path(event), class: "m-b-xs" do 
                      event.title
                    end
                    para do 
                      event.matter ? event.matter.title : event.note.html_safe
                    end
                    div class: "stat-percent project-people" do 
                      user_photos(event.users).html_safe
                    end
                  end
                end
              end
            end
          end
        end
      end
      div class: "col-md-6" do 
        div class: :ibox do 
          div class: "ibox-content" do 
            h2 "Сүүлийн комментууд"
            div class: "chat-discussion" do 
              ActiveAdmin::Comment.order("-id").take(10).each do |comment| 
                div for: comment, class: "chat-message" do
                  text_node image_tag  comment.author.photo.url(:small), class: "message-avatar"
                  div class: 'message' do
                    a href: admin_admin_user_path(comment.author), class: "message-author" do 
                      comment.author.name
                    end
                    span class: "message-date" do 
                      a href: auto_url_for(comment.resource) do 
                        text_node pretty_format(comment.created_at)
                      end
                    end
                    span class: 'message-content' do
                      simple_format comment.body
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
   
  end # content
end
