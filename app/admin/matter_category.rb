ActiveAdmin.register MatterCategory do 
	menu parent: "Тохиргоо", label: "Гэрээний ангилал"
	form do |f|
		f.inputs do
			div class: :row do 
				f.input :code, :wrapper_html => {class: "col-sm-6 "}
				f.input :name, :wrapper_html => {class: "col-sm-6"}
				f.input :parent, :wrapper_html => {class: "col-sm-6"}, input_html: {class: "form-control chosen-select"}
				f.input :status, wrapper_html: {class: "col-sm-6"}
				f.input :note, wrapper_html: {class: "col-sm-12"}, input_html: {class: "form-control summernote"}
			end
		end 
		f.actions
	end

	controller do
    def permitted_params
      params.permit!
      # params.permit! # allow all parameters
    end
	end

	index do 
		column :code do |obj|
			link_to obj.code, admin_matter_category_url(obj)
		end
		column :name do |obj|
			auto_link obj
		end
		column :status do |obj|
			status_tag  t("enums.default.#{obj.status}"), obj.enabled? ? :primary : :default
		end
		column :parent
	end
end