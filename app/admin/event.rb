ActiveAdmin.register Event do 
	
	menu icon: "fa fa-clock-o", label: "Ажил төлөвлөлт", priority: 4

	scope :all
	scope "Open" do |obj| 
		obj.open
	end 

	scope "Completed" do |obj| 
		obj.completed
	end 

	scope "Canceled" do |obj| 
		obj.canceled
	end 


	filter :customer
	filter :matter
	filter :users
	filter :category
	filter :title
	filter :start_date
	filter :due_date

	index title: "Ажил төлөвлөлт" do
		column "Төлөв", sortable: :status do |obj|
			div class: "m-b-xs" do
				status_tag t("enums.default.#{obj.status}"), obj.status_class
				span "#{obj.progress}%", class: "pull-right"  
			end
			div class: "progress progress-mini progress-striped no-margins" do 
				div class: "progress-bar", style: "width: #{obj.progress}%;"
			end
		end
		column "Ажил",sortable: :title do |obj|
			a href:  admin_event_path(obj) do 
				 text_node obj.category.name
			end
			br
			content_tag(:small, obj.title)
		end
		column "Эхлэх огноо",:start_date
		column "Дуусах огноо",:due_date
		column "Харилцагч", :customer
		column "Гэрээ", :matter
		column "Ажилтан", class: "project-people" do |obj|
			user_photos(obj.users).html_safe
		end
	end

	form title: "Ажил бүртгэл" do |f|
		f.object.customer_id = params[:customer_id]  if params[:customer_id]
		if params[:matter_id] 
			f.object.matter_id = params[:matter_id]
			f.object.customer_id = Matter.find(params[:matter_id]).customer_id
		end
		div class: :row do 
			div class: "col-sm-6" do
				f.inputs do
					div class: :row do 
						unless params[:type] and params[:type] == "task" 
							f.input :customer, wrapper_html: {class: "col-sm-6"}, input_html: {class: "chosen-select form-control matter-customer"}, label: "Харилцагч"
							if params[:customer_id]
								f.input :matter, wrapper_html: {class: "col-sm-6"}, input_html: {class: "chosen-select form-control matter-matter"}, label: "Гэрээ",
									collection: Matter.where(customer_id: params[:customer_id]).collect {|m| [m.code, m.id]}
							else
									f.input :matter, wrapper_html: {class: "col-sm-6"}, input_html: {class: "chosen-select form-control matter-matter"}, label: "Гэрээ"
							end
						end
						f.input :category, wrapper_html: {class: "col-sm-12"}, input_html: {class: "chosen-select form-control"}, label: "Ажлын төрөл"
						f.input :title, wrapper_html: {class: "col-sm-12"}, label: "Ажил"
						f.input :start_date, wrapper_html: {class: "col-sm-6"}, as: :datetime_picker, label: "Эхлэх огноо"
						f.input :due_date, wrapper_html: {class: "col-sm-6"}, as: :datetime_picker, label: "Дуусах огноо"
						f.input :users, input_html: {class: "chosen-select form-control"}, label: "Хариуцагч", wrapper_html: {class: "col-sm-6"}
						f.input :progress, input_html: {class: "form-control"}, label: "Гүйцэтгэл", wrapper_html: {class: "col-sm-6"}, as: :select, 
							collection: 21.times.collect { |i| ["#{i*5}%" , i*5]} 

					end
				end
			end
			div class: "col-sm-6" do 
				f.inputs do
					f.input :note, input_html: {class: "form-control summernote"}, label: "Тэмдэглэл"
				end
			end
		end
		f.actions
	end

	
	show do 
		div class: :row do 
			div class: "col-lg-6" do
				attributes_table "" do 
					row "Харилцагч" do 
						event.customer
					end
					row  "Гэрээ" do  
						event.matter
					end
					row "Ажлын төрөл" do 
						event.category
					end
					row "Ажил" do 
						event.title
					end
					row "Эхлэх огноо" do 
						event.start_date
					end
					row "Дуусах огноо"  do 
						event.due_date
					end
					row "Төлөв" do 
						status_tag t("enums.default.#{event.status}"), event.status_class
					end
					row "Хариуцагч",class: "project-people" do 
						user_photos(event.users).html_safe
					end
					row "Гүйцэтгэл" do 
						div class: "m-b-xs" do
							span "#{event.progress}%", class: ""  
						end
						div class: "progress progress-small progress-striped no-margins" do 
							div class: "progress-bar", style: "width: #{event.progress}%;"
						end
					end
				end 
			end
			div class: "col-lg-6" do
				panel "Тэмдэглэл" do 
					raw event.note
				end 
			end
			div class: "col-lg-12" do 
				active_admin_comments
			end
		end
	end
	member_action :calendar, method: :get do 
		@events = Event.all
		res = []
		@events.each do |event|
			data = {
				title: "#{event.title} - #{event.customer.name}(#{event.matter.code})",
				start: event.start_date,
				end: event.due_date
			} 
			res << data
		end
		respond_to do |format|
			format.json {render json: res }
		end
	end

	member_action :complete, method: :get do 
		if resource.completed!
			redirect_to admin_event_path(resource), notice: "Мэдээлэл шинэчилэгдлээ!"
		end
	end
	member_action :cancel, method: :get do 
		if resource.canceled!
			redirect_to admin_event_path(resource), notice: "Мэдээлэл шинэчилэгдлээ!"
		end
	end

	action_item :action_complete, only: :show do
		if resource.open?
			a href: complete_admin_event_path(resource), class: "btn btn-primary" do 
				i class: "fa fa-check" 
				text_node "Complete"
			end
			a href: cancel_admin_event_path(resource), class: "btn btn-danger" do 
				i class: "fa fa-times"
				text_node "Cancel"
			end
		end
	end 
	controller do
    def permitted_params
      params.permit!
      # params.permit! # allow all parameters
    end
	end
end