ActiveAdmin.register Customer do 
	menu icon: "fa fa-group", priority: 2, label: "Харилцагч"

	scope :all

	scope :personal do |obj| 
		obj.personal
	end 
	scope :company do |obj| 
		obj.company
	end 
	
	filter :code
	filter :name
	filter :title
	filter :register_no
	filter :email
	filter :phone

	form title: "Харилцагчийн бүртгэл" do |f|
		f.object.customer_type = params[:customer_type] if params[:customer_type]
		div class: :row do 
			div class: "col-sm-12" do 
				f.inputs "Харилцагчийн төрөл" do
					div class: :row do 
						f.input :customer_type, wrapper_html: {class: "col-sm-4"}, collection: [["Иргэн","personal"], ["Байгууллага","company"]], 
							input_html: {class: "form-control customer-changer"}, label: "Харилцагчийн төрөл"
						f.input :photo, wrapper_html: {class: "col-sm-6"}, label: "Харилцагчийн зураг"
					end
				end
			end
			div class: "col-sm-6" do 
				if f.object.personal?
					f.inputs "Харилцагчийн мэдээлэл (Иргэн)" do 
						div class: :row do 
							f.input :country, as: :country, wrapper_html: {class: "col-md-6"}, label: "Иргэншил", priority_countries: ["MN"],input_html: {class: "form-control chosen-select"}
							f.input :surname, wrapper_html: {class: "col-md-6"}, label: "Ургийн овог"
							f.input :title , wrapper_html: {class: "col-sm-6"}, label: "Овог"
							f.input :name , wrapper_html: {class: "col-sm-6"}, label: "Нэр"
							f.input :birth_date, as: :date_picker, wrapper_html: {class: "col-sm-6"}, label: "Төрсөн огноо"
							f.input :register_no , wrapper_html: {class: "col-sm-6"} ,label: "Регистерийн дугаар"
							f.input :email,  wrapper_html: {class: "col-sm-6"}, label: "И-мэйл хаяг"
							f.input :phone, as: :phone,  wrapper_html: {class: "col-sm-6"}, label: "Утас"
							f.input :job, wrapper_html: {class: "col-sm-6"}, label: "Ажил"
							f.input :gender,  wrapper_html: {class: "col-sm-6"}, label: "Хүйс", collection: [["Эрэгтэй","male"], ["Эмэгтэй","female"]]

						end
					end
				elsif f.object.company? 
					f.inputs "Харилцагчийн мэдээлэл (Байгууллага)" do 
						div class: :row do 
							f.input :country, as: :country, wrapper_html: {class: "col-md-6"}, label: "Харьяалал", priority_countries: ["MN"], input_html: {class: "form-control chosen-select"}
							f.input :name , wrapper_html: {class: "col-sm-6"}, label: "Байгууллагын нэр"
							f.input :birth_date, as: :date_picker, wrapper_html: {class: "col-sm-6"}, label: "Байгуулагдсан огноо" 
							f.input :register_no , wrapper_html: {class: "col-sm-6"}, label: "Регистерийн дугаар"
							f.input :email,  wrapper_html: {class: "col-sm-6"},  label: "И-мэйл хаяг"
							f.input :phone, as: :phone,  wrapper_html: {class: "col-sm-6"},  label: "Утас"
							f.input :job, wrapper_html: {class: "col-sm-12"}, label: "Үйл ажиллагааны чиглэл"
						end
					end
				end
			end
			div class: "col-sm-6" do 
				f.inputs "Тэмдэглэл" do
					f.input :note, input_html: {class: "form-control summernote"}, label: false
				end
			end
		end
		f.inputs "Холбоо барих" do
			f.has_many :contacts, allow_destroy: true  do |c|
				c.input :contact_type, wrapper_html: {class: "col-sm-2"}
				c.input :address_line, wrapper_html: {class: "col-sm-2"}
				c.input :address_line1, wrapper_html: {class: "col-sm-2"}
				c.input :address_line2, wrapper_html: {class: "col-sm-2"}
				c.input :phone,wrapper_html: {class: "col-sm-2"}
			end
		end
		f.actions
	end

	index title: "Харилцагчийн бүртгэл" do 
		column "Төлөв", sortable: :status do |obj| 
			status_tag t("enums.default.#{obj.status}"), obj.status_class
		end
		column "Код", sortable: :code do |obj|
			(obj.code + "<br/>" + content_tag(:small, obj.personal? ? "Иргэн" : "Байгууллага")).html_safe
		end
		column "", class: "project-people" do |obj|
			img src: obj.photo.url(:small), class: "img-circle"
		end
		column "Харилцагч", sortable: :name  do |obj|
			link_to obj.name, admin_customer_url(obj)
		end

		column "Регистер", :register_no
		column "И-мэйл", :email
		column "Утас", :phone
		column "Ажил", :job
	
	end

	show do 
		div class: :row do 
			div class: "col-md-4" do 
				div class: "ibox float-e-margins" do 
					div class: "ibox-title" do 
						h5 "Харилцагчийн зураг"
					end
					div class: "ibox-content no-padding border-left-right" do 
						img src: customer.photo.url(:medium), class: "img-responsive", style: "margin: auto;"
					end
				end
			end
			div class: "col-md-4" do 
				if customer.personal?
					attributes_table "Харилцагчийн мэдээлэл (Иргэн)" do 
						row "Код" do 
							customer.code
						end
						row "Ургийн овог" do 
							customer.surname
						end
						row "Харилцагчийн нэр" do 
							[customer.title, customer.name].join(" ")
						end
						row "Регистерийн дугаар" do 
							customer.register_no
						end
						row "Төрсөн огноо" do
							"#{customer.birth_date} (#{customer.male? ? 'Эрэгтэй' : 'Эмэгтэй' })" 
						end
						row customer.personal? ? "Ажил" : "Үйл ажиллагаа" do 
							customer.job
						end
					end
				else
					attributes_table "Харилцагчийн мэдээлэл (Байгууллага)" do 
						row "Код" do 
							customer.code
						end
						row "Харилцагчийн нэр" do 
							customer.name
						end
						row "Регистерийн дугаар" do 
							customer.register_no
						end
						row "Байгууллагадсан огноо" do
							customer.birth_date
						end
						row customer.personal? ? "Ажил" : "Үйл ажиллагаа" do 
							customer.job
						end
					end
				end
			end
			div class: "col-md-4" do 
				attributes_table "Харилцагчийн мэдээлэл" do 
					row "Харьяалал & Иргэншил" do 
						country = ISO3166::Country[customer.country||"MN"]
						text_node country.translations[I18n.locale.to_s] || country.name
					end
					row "Утас" do
						customer.phone
					end
					row "И-мэйл хаяг" do 
						a href: "mailto:#{customer.email}" do 
							i class: "fa fa-envelope"
							text_node customer.email
						end
					end 
					row "Төлөв" do 
						status_tag  t("enums.default.#{customer.status}"), customer.status_class
					end 
					row "Бүртгэсэн огноо" do 
						customer.created_at
					end
					row "Шинэчилсэн огноо" do 
						customer.updated_at
					end
				end
			end
			div class: "col-sm-12" do
				tabs do 
					tab "Хаяг", class: "active" do 
						div class: :row do 
							div class: "col-sm-12" do 
								table_for customer.contacts, class: "table table-hover issue-tracker" do 
									column do 
									end
									column "Төрөл" do |obj|
										i class: "fa fa-map-marker fa-2x m-r"
										text_node obj.contact_type
									end
									column "Хаяг" do |obj|
										[obj.address_line, obj.address_line1, obj.address_line2].join(" ")
									end
									column "Утас", :phone
								end
							end
						end
					end
					tab "Тэмдэглэл" do 
						div class: :row  do 
							div class: "col-sm-12" do 
								panel "Тэмдэглэл" do 
									raw customer.note
								end
							end
						end
					end
					tab "Гэрээ" do 
						div class: :row do 
							div class: "col-sm-12" do 
								a href: new_admin_matter_path(customer_id: customer.id), class: "btn btn-primary btn-sm m-t m-l" do
									i class: "fa fa-plus m-r"
									text_node "Гэрээ нэмэх"
								end
							end
							div class: "col-sm-12" do 
								table_for customer.matters, class: "table table-hover issue-tracker" do 
									column "Төлөв", sortable: :status do |obj| 
										status_tag t("enums.default.#{obj.status}"), obj.status_class
									end
									column "Код", sortable: :code do |obj|
										link_to obj.code, admin_matter_url(obj)
									end
									column "Агуулга" do |obj|
										(obj.category.name + "<br/>" + 
										content_tag(:small, obj.title)).html_safe
									end
									column "Эхлэх огноо", :start_date
									column "Дуусах огноо", :due_date
								end
							end
						end
					end
					tab "Ажил төлөвлөлт" do 
						div class: :row do 
							div class: "col-sm-12" do 
								a href: new_admin_event_path(customer_id: customer.id), class: "btn btn-primary btn-sm m-t m-l" do
									i class: "fa fa-plus m-r"
									text_node "Ажил нэмэх"
								end
							end
							div class: "col-sm-12" do 
								table_for customer.events, class: "table table-hover issue-tracker" do
									column "Төлөв", sortable: :status do |obj|
										status_tag t("enums.default.#{obj.status}"), obj.status_class
									end
									column "#", sortable: :id do |obj|
										link_to "##{obj.id}", edit_admin_event_path(obj)
									end
									column "Ажил", sortable: :title do |obj|
										(obj.category.name + "<br/>" + 
										content_tag(:small, obj.title)).html_safe
									end
									column "Эхлэх огноо",:start_date
									column "Дуусах огноо",:due_date
									column "Гэрээ", :matter
									column "Ажилтан", class: "project-people" do |obj|
										user_photos(obj.users).html_safe
									end
								end
							end
						end
					end
					tab "Төлбөр" do 
						div class: :row do 
							div class: "col-sm-12" do 
								a href: new_admin_payment_path(customer_id: customer.id), class: "btn btn-primary btn-sm m-t m-l" do
									i class: "fa fa-plus m-r"
									text_node "Төлбөр нэмэх"
								end
							end
							div class: "col-sm-12" do 
								table_for  customer.payments, class: "table table-hover issue-tracker" do 
									column "Төлөв", sortable: :status do |obj|
										status_tag t("enums.default.#{obj.status}"), obj.status_class
									end

									column "Код",sortable: :code do |obj|
										text_node link_to obj.code, admin_payment_url(obj) 
										br
										span obj.title, class: "small"
									end
									column "Гэрээ" , :matter
									column "Валют", :currency
									column "Төлбөрийн дүн" , sortable: :amount do |obj|
										number_format obj.amount
									end
									column "Төлөгдсөн дүн", sortable: :amount_paid do |obj|
										paid_amount = obj.transactions.authorized.sum(:amount) 
										paid_percent = paid_amount == 0 ? 0 :  paid_amount * 100 / obj.amount
										div do
											span number_format paid_amount
											small "#{paid_percent}%", class: "pull-right"  
										end
										div class: "progress progress-mini no-margins" do 
											div class: "progress-bar", style: "width: #{paid_percent}%;"
										end
									end
									column "Үлдэгдэл" do |obj| 
										number_format obj.balance
									end 
									column "Төлөгдөх огноо" , :due_date
								end
							end
						end
					end
					tab "Хавсралт" do 
						div class: :row do 
							div class: "col-sm-12" do 
								a href: new_admin_document_path(customer_id: customer.id), class: "btn btn-primary btn-sm m-t m-l" do
									i class: "fa fa-plus m-r"
									text_node "Хавсралт нэмэх"
								end
							end
							div class: "col-sm-12" do 
								table_for customer.documents, class: "table table-striped" do 
									column :title
									column :file do |obj|
										link_to obj.file_file_name, download_admin_document_path(obj)
									end
									column "Төрөл" do |obj|
										obj.file_content_type
									end
									column :file_file_size 
								end
							end
						end
					end
					tab "Коммент" do 
						div class: :row do 
							div class: "col-sm-12" do 
								active_admin_comments
							end
						end
					end
				end
			end
		end
	end
	action_item :status, only: :show do
		if customer.active?  
			link_to "Mark as Inactive", change_status_admin_customer_path(customer), class: "btn btn-default"
		else
			link_to "Mark as Active", change_status_admin_customer_path(customer), class: "btn btn-primary"
		end
	end

	member_action :change_status do 
		resource.active? ? resource.inactive!	: resource.active!
		redirect_to admin_customer_path(resource)		
	end

  member_action :search do
  	@customer = Customer.search(params[:search])
  	render action: :search
  end

  member_action :by_customer, method: :get do 
		matters = Matter.where(:customer => resource)
		res = []
		matters.each do |m|
			data = {
				value: m.id,
				text: m.code
			} 
			res << data
		end

		respond_to do |format|
			format.json {render json: res }
		end
	end

	controller do
	#	authorize_resource
    def permitted_params
      params.permit!
      # params.permit! # allow all parameters
    end
	end
end