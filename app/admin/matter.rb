ActiveAdmin.register Matter do 
	menu icon: "fa fa-sign-in", priority: 3, label: "Гэрээ"

	scope :all
	scope :open do |obj|
		obj.open
	end
	scope :pending do |obj|
		obj.pending
	end
	scope :solved do |obj|
		obj.solved
	end

  filter :customer
  filter :category
  filter :code
  filter :title
  filter :start_date
  filter :due_date


	form title: "Гэрээ бүртгэл" do |f|
		f.object.customer_id = params[:customer_id] if params[:customer_id]

		div class: :row do 
			div class: "col-sm-6" do 
				f.inputs "Ерөнхий мэдээлэл" do 
					div class: :row do 
						f.input :customer, wrapper_html: {class: "col-sm-12"}, input_html: {class: "form-control chosen-select"}, label: "Харилцагч"
						f.input :category, collection: option_groups_from_collection_for_select(MatterCategory.all, :childs, :name, :id, :name ), :group_by => :parent,
							wrapper_html: {class: "col-sm-12"}, input_html: {class: "form-control chosen-select"}, label: "Ангилал"

						f.input :title, wrapper_html: {class: "col-sm-12"}, label: "Агуулга"
						f.input :start_date,  as: :date_picker, wrapper_html: {class: "col-sm-6"}, label: "Эхлэх огноо"
						f.input :due_date,  as: :date_picker, wrapper_html: {class: "col-sm-6"}	, label: "Дуусах огноо"
					end
				end 
			end
			div class: "col-sm-6" do
				f.inputs "Нэмэлт тэмдэглэл" do 
					f.input :note, :label => false, input_html: {class: "form-control summernote" }
				end 
			end
		end
		f.actions
	end

	index title: "Гэрээ бүртгэл" do 
		column "Төлөв", sortable: :status do |obj| 
			status_tag t("enums.default.#{obj.status}"), obj.status_class
		end
		column "Код", sortable: :code do |obj|
			link_to obj.code, admin_matter_url(obj)
		end
		column "", class: "project-people" do |obj|
			img src: obj.customer.photo.url(:small), class: "img-circle"	if obj.customer 

		end
		column "Харилцагч", sortable: :name  do |obj|
			link_to obj.customer.name, admin_customer_url(obj.customer) if obj.customer
		end
		column "Агуулга" do |obj|
			(obj.category.name + "<br/>" + 
			content_tag(:small, obj.title)).html_safe
		end
		column "Эхлэх огноо", :start_date
		column "Дуусах огноо", :due_date
	end

	show do 
		div class: :row do 
			div class: "col-sm-6" do 
				attributes_table "Гэрээний мэдээлэл" do
					row "Код" do 
						matter.code
					end 
					row "Харилцагч" do 
						link_to matter.customer.name, admin_customer_path(matter.customer)
					end
					row "Ангилал" do 
						link_to matter.category.name, admin_matter_category_path(matter.category)
					end 
					row "Агуулга" do 
						matter.title
					end 
				end
			end
			div class: "col-sm-6" do 
				attributes_table "Гэрээний мэдээлэл" do
					row "Төлөв" do 
						status_tag t("enums.defaults.#{matter.status}"), matter.status_class
					end
					row "Эхлэх огноо" do 
						matter.start_date
					end
					row "Дуусах огноо" do 
						matter.due_date
					end
					row "Бүртгэсэн огноо" do 
						matter.created_at
					end
				end
			end

			div class: "col-sm-12" do
				tabs do 
					tab "Тэмдэглэл", class: "active" do 
						div class: :row  do 
							div class: "col-sm-12" do 
								panel "Тэмдэглэл" do 
									raw matter.note
								end
							end
						end
					end
					tab "Ажил төлөвлөлт" do 
						div class: :row do 
							div class: "col-sm-12" do 
								a href: new_admin_event_path(matter_id: matter.id), class: "btn btn-primary btn-sm m-t m-l" do
									i class: "fa fa-plus m-r"
									text_node "Ажил бүртгэх"
								end
							end
							div class: "col-sm-12" do 
								table_for matter.events, class: "table table-hover issue-tracker" do 
									column "Төлөв", sortable: :status do |obj|
										status_tag  t("enums.default.#{obj.status}"), obj.status_class
									end
									column "#", sortable: :id do |obj|
										link_to "##{obj.id}", edit_admin_event_path(obj)
									end
									column "Ажил", sortable: :title do |obj|
										(obj.category.name + "<br/>" + 
										content_tag(:small, obj.title)).html_safe
									end
									column "Эхлэх огноо",:start_date
									column "Дуусах огноо",:due_date
									column "Ажилтан", class: "project-people" do |obj|
										user_photos(obj.users).html_safe
									end
								end
							end
						end
					end
					tab "Төлбөр" do 
						div class: :row do 
							div class: "col-sm-12" do 
								a href: new_admin_payment_path(matter_id: matter.id), class: "btn btn-primary btn-sm m-t m-l" do
									i class: "fa fa-plus m-r"
									text_node "Төлбөр бүртгэх"
								end
							end
							div class: "col-sm-12" do 
								table_for  matter.payments, class: "table table-hover issue-tracker" do 
									column "Төлөв", sortable: :status do |obj|
										status_tag t("enums.default.#{obj.status}"), obj.status_class
									end

									column "Код",sortable: :code do |obj|
										text_node link_to obj.code, admin_payment_url(obj) 
										br
										span obj.title, class: "small"

									end
									column "Валют", :currency
									column "Төлбөрийн дүн" , sortable: :amount do |obj|
										number_format obj.amount
									end
									column "Төлөгдсөн дүн", sortable: :amount_paid do |obj|
										paid_amount = obj.transactions.authorized.sum(:amount) 
										paid_percent = paid_amount == 0 ? 0 :  paid_amount * 100 / obj.amount
										div do
											span number_format paid_amount
											small "#{paid_percent}%", class: "pull-right"  
										end
										div class: "progress progress-mini no-margins" do 
											div class: "progress-bar", style: "width: #{paid_percent}%;"
										end
									end
									column "Үлдэгдэл" do |obj| 
										number_format obj.balance
									end 
									column "Төлөгдөх огноо" , :due_date
								end
							end
						end
					end
					tab "Хавсралт" do 
						div class: :row do 
							div class: "col-sm-12" do 
								a href: new_admin_document_path(matter_id: matter.id), class: "btn btn-primary btn-sm m-t m-l" do
									i class: "fa fa-plus m-r"
									text_node "Хавсралт нэмэх"
								end
							end
							div class: "col-sm-12" do 
								table_for matter.documents, class: "table table-striped" do 
									column :title
									column :file do |obj|
										link_to obj.file_file_name, download_admin_document_path(obj)
									end
									column "Төрөл" do |obj|
										obj.file_content_type
									end
									column :file_file_size 
								end
							end
						end
					end
					tab "Коммент" do 
						div class: :row do 
							div class: "col-sm-12" do 
								active_admin_comments
							end
						end
					end
				end
			end
		end
	end

	action_item :action_pending, only: :show do 
		if matter.open?
			link_to 'Set Pending', change_status_admin_matter_path(matter, status: "pending"), class: "btn btn-warning" 
	 	end
	end
	action_item :action_cancel, only: :show do 
		if matter.open? || matter.pending?
			link_to 'Cancel', change_status_admin_matter_path(matter, status: :cancel), class: "btn btn-danger" 
	 	end
	end

	action_item :action_solved, only: :show do 
		if matter.pending?
			link_to 'Solved', change_status_admin_matter_path(matter, status: :solved), class: "btn btn-primary" 
	 	end
	end
		action_item :action_open, only: :show do 
		if matter.solved? || matter.canceled?
			link_to 'Re-Open', change_status_admin_matter_path(matter, status: :open), class: "btn btn-success" 
	 	end
	end

	member_action :change_status, method: :get do
		if params[:status] == "open"
			resource.open!
		elsif params[:status] == "pending"
			resource.pending!
		elsif params[:status] == "cancel"
			resource.canceled!
		elsif params[:status] == "solved"
			resource.solved!
		end
		redirect_to admin_matter_path(resource)
	end

	controller do
    def permitted_params
      params.permit!
      # params.permit! # allow all parameters
    end
	end
end