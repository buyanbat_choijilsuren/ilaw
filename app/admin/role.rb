ActiveAdmin.register_page "Role" do

  menu priority: 1, label: "Хэрэглэгчийн эрх", parent: "Тохиргоо"

  content title: "Role" do
    div class: :row do
      div class: "col-md-12 " do 
        div class: "ibox" do 
          div class: "ibox-content" do 
          	Role.all.each do |role| 
          		div class: "panel panel-info" do 
          			div class: "panel-heading" do 
          				h4 class: "panel-title" do 
                    a href: "#rcl-#{role.id}", "data-toggle": "collapse", "data-parent": "#accordion" do 
                      i class: "fa fa-chevron-down"
                      text_node role.name
                    end
                  end
          			end
          			div class: "panel-collapse collapse", id: "rcl-#{role.id}" do
          				form action: "/admin/role/update", method: :post do |f|
          					f.input :name => 'authenticity_token', :type => :hidden, :value => form_authenticity_token.to_s
          					f.input type: :hidden, name: :role_id, value: role.id
	          				div class: "panel-body" do 
		          				table class: "table table-bordered table-striped" do 
		          					thead do 
		          						th "Object"
		          						Permission::CONST_ACTIONS.each do |k,v|  
		          							th v, class: "text-center"
		          						end
		          					end
			          				Permission::CONST_CLASSES.each do |k,v|
			          					tr do 
			          						td v
			          						current_perm = Permission.where(role: role, subject_class: k).first
			          						current_perm ||= Permission.new

														Permission::CONST_ACTIONS.each do |ak,av|  
			          							td align: :center do 
			          								if current_perm.action == ak
			          									f.input class: "i-checks", type: "radio", name: "roles[#{k}]", value: ak, checked: true
			          								else
			          									f.input class: "i-checks", type: "radio", name: "roles[#{k}]", value: ak
			          								end
			          							end
			          						end
			          					end
			          				end
			          			end
			          		end
			          		div class: "panel-footer text-right" do 
			          			a "Болих", class: "btn btn-default", href: admin_role_path
			          			f.input value: "Хадгалах", type: :submit, class: "btn btn-primary"
			          		end
			          	end #form
          			end
          		end
          	end
          end
        end
      end
    end
  end # content

  page_action :update, method: :post do 
  	@role = Role.find(params[:role_id])

  	Permission::CONST_CLASSES.each do |k,v|
  		permission = @role.permissions.where(subject_class: k).first
  		permission ||= Permission.new(role_id: @role.id, subject_class: k) 
  		permission.action = params[:roles][k]
  		permission.save
  	end

  	redirect_to admin_role_path, notice: "Мэдээлэл шинэчилэгдлээ!"
  end
end
