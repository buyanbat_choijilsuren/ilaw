ActiveAdmin.register PublicActivity::Activity do 
	menu parent: "Тохиргоо", label: "Лог"

	collection_action :get_latests, method: :get do 
		@activities = PublicActivity::Activity.order(id: :desc).take(10)
		render layout: false
	end

	collection_action :seen, method: :get do 
		user = current_admin_user
		user.actvity_checked = [user.actvity_checked, params[:activity_id].to_i || 0].max
		user.save
		render json: {result: :ok}
	end
end

ActiveAdmin.register Reminder do 
	menu parent: "Тохиргоо", label: "Reminder"

	collection_action :check, method: :get do 
		@reminders = Reminder.open.where(user: current_admin_user)
		render layout: false
	end

	collection_action :seen, method: :get do 
		reminder = Reminder.find(params[:reminder_id])

		if reminder and reminder.user == current_admin_user 
			reminder.seen_date = Time.now
			reminder.seen!
		end
		render json: {result: :ok}
	end
end