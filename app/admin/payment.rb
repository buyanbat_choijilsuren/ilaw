ActiveAdmin.register Payment do
	menu icon: "fa fa-bar-chart-o", priority: 6, label: "Төлбөр"

	filter :customer
	filter :matter
	filter :code
	filter :title
	filter :amount
	filter :amount_paid
	filter :due_date

	form title:"Төлбөр бүртгэл" do |f|
		if params[:customer_id]
			f.object.customer_id = params[:customer_id] 
		end
		if params[:matter_id]
			f.object.matter_id = params[:matter_id]
			f.object.customer_id = Matter.find(params[:matter_id]).customer_id
		end

		div class: :row do 
			div class: "col-sm-8" do 
				f.inputs do
					div class: :row do 
						f.input :customer, wrapper_html: {class: "col-sm-6"}, input_html: {class: "form-control chosen-select matter-customer"}, label: "Харилцагч"
						if params[:customer_id]
							f.input :matter,  wrapper_html: {class: "col-sm-6"}, input_html: {class: "form-control chosen-select matter-matter"}, 
								collection: Matter.where(customer_id: params[:customer_id]).collect {|m| [m.code, m.id]}, label: "Гэрээ"
						else
							f.input :matter,  wrapper_html: {class: "col-sm-6"}, input_html: {class: "form-control chosen-select matter-matter"}, label: "Гэрээ"
						end
						f.input :title,  wrapper_html: {class: "col-sm-6"}, label: "Төлбөрийн Утга"
						f.input :due_date, as: :date_picker,  wrapper_html: {class: "col-sm-6"}, label: "Төлбөр хийх огноо"
					end
				end
			end
			div class: "col-sm-4" do 
				f.inputs do 
					f.input :currency, as: :select, collection: Payment.currencies, label: "Төлбөрийн валют"
					f.input :amount, label: "Төлбөрийн дүн"
				end
			end
		end
		f.actions
	end

	scope :all
	scope "Open" do |obj| 
		obj.open
	end 
	scope "Paid" do |obj| 
		obj.paid
	end 
	scope "Canceled" do |obj| 
		obj.canceled
	end 

	index title: "Төлбөрийн мэдээлэл" do
		column "Төлөв", sortable: :status do |obj|
			status_tag t("enums.default.#{obj.status}"), obj.status_class
		end

		column "Код",sortable: :code do |obj|
			text_node link_to obj.code, admin_payment_url(obj) 
			br
			span obj.title, class: "small"

		end
		column "Харилцагч", :customer, sortable: :matter_id
		column "Гэрээ" , :matter, sortable: :matter_id
		column "Валют", :currency
		column "Төлбөрийн дүн" , sortable: :amount do |obj|
			number_format obj.amount
		end
		column "Төлөгдсөн дүн", sortable: :amount_paid do |obj|
			paid_amount = obj.transactions.authorized.sum(:amount) 
			paid_percent = paid_amount == 0 ? 0 :  paid_amount * 100 / obj.amount
			div do
				span number_format paid_amount
				small "#{paid_percent.to_i}%", class: "pull-right"  
			end
			div class: "progress progress-mini no-margins" do 
				div class: "progress-bar", style: "width: #{paid_percent}%;"
			end
		end
		column "Үлдэгдэл" do |obj| 
			number_format obj.balance
		end 
		column "Төлөгдөх огноо" , :due_date
	end

	show do
		div class: :row do
			div class: "col-sm-6" do 
				attributes_table "Төлбөрийн мэдээлэл" do 
					row "Код" do
						payment.code
					end
					row "Харилцагч" do 
						payment.customer
					end
					row "Гэрээ" do 
						payment.matter
					end
					row "Төлбөрийн Утга" do
						payment.title
					end
					row "Төлөгдөх огноо" do
						payment.due_date
					end
				end
			end 
			div class: "col-sm-6" do 
				attributes_table "Төлбөрийн мэдээлэл" do 
					row "Төлөв" do
						status_tag  t("enums.default.#{payment.status}"), payment.status_class
					end
					row "Төлбөрийн валют" do 
						payment.currency	
					end
					row "Төлбөрийн дүн" do 
						number_format payment.amount
					end
					row "Төлөгдсөн дүн" do 
						paid_amount = payment.transactions.authorized.sum(:amount) 
						paid_percent = paid_amount == 0 ? 0 :  paid_amount * 100 / payment.amount
						div do
							span number_format paid_amount
							small "#{paid_percent}%", class: "pull-right"  
						end
						div class: "progress progress-small progress-striped no-margins" do 
							div class: "progress-bar", style: "width: #{paid_percent}%;"
						end
					end
					row "Төлбөрийн үлдэгдэл" do
						number_format payment.balance
					end
				end
			end 
			div class: "col-sm-12" do
				tabs do 
					tab "Төлөлт", class: "active" do 
						table_for payment.transactions, class: "table table-hover issue-tracker" do 
							column "Төлөв" do |obj|
								status_tag  t("enums.default.#{obj.status}"), obj.status_class
							end
							column "Код" do |obj|
								strong obj.code
							end

							column "Утга" do |obj|
								text_node obj.title
								br
								small obj.description
							end
							column "Валют",:currency
							column "Дүн" do |obj|
								number_format obj.amount
							end
							column "Төлөлт хийсэн огноо",:trn_date
							column "" do |obj|
								if obj.open?
									a href: authorize_admin_payment_transaction_url(obj), class: "btn btn-primary btn-sm" do 
										i class: "fa fa-check m-r-sm"
										text_node "Authorize"
									end
									a href: cancel_admin_payment_transaction_url(obj), class: "btn btn-danger btn-sm" do 
										i class: "fa fa-times m-r-sm"
										text_node "Cancel"
									end
								end
							end
						end
					end
					tab "Комментууд" do 
						active_admin_comments
					end
				end
			end
		end 
	end

	action_item :add_transaction, only: :show do 
		if resource.open?
			a href: new_admin_payment_transaction_path(payment: payment.id), class: "btn btn-primary" do 
				i class: "fa fa-plus m-r"
				text_node "Төлөлт бүртгэх"
			end
		end
	end

	controller do
    def permitted_params
      params.permit!
      # params.permit! # allow all parameters
    end
	end

end

ActiveAdmin.register PaymentTransaction do 
	menu false
	form do |f|
		if params[:payment] 
			payment = Payment.find(params[:payment])
			f.object.payment_id = payment.id
			f.object.currency = payment.currency
			f.object.title = payment.title
			f.object.amount = payment.balance
			f.object.trn_date = Time.now
		end
		div class: :row do 
			div class: "col-md-6" do 
				f.inputs "" do 
					f.input :payment, input_html: {class: "form-control chosen-select"}, collection: Payment.open.map {|p| ["#{p.code} #{p.title}", p.id]}, label: "Төлбөр"
					f.input :title, label: "Төлбөрийн Утга"
					f.input :description, label: "Нэмэлт тэмдэглэл"
				end
			end
			div class: "col-md-6" do 
				f.inputs "" do 
					f.input :currency, as: :select, collection: Payment.currencies, label: "Валют"
					f.input :amount, label: "Дүн"
					f.input :trn_date, as: :date_picker, label: "Төлбөрийн огноо"
				end	
			end
		end
		f.actions
	end

	member_action :authorize, method: :get do 
		trn = PaymentTransaction.find(params[:id])
		trn.authorize
		redirect_to admin_payment_path(trn.payment)
	end

	member_action :cancel, method: :get do 
		trn = PaymentTransaction.find(params[:id])
		trn.canceled!
		redirect_to admin_payment_path(trn.payment)
	end

	controller do 
		def permitted_params
			params.permit!
		end

		def create
			create! do |format|
      	format.html { redirect_to admin_payment_path resource.payment } if resource.valid?
      end
		end
	end
end