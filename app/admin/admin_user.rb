ActiveAdmin.register AdminUser do

  menu parent: "Тохиргоо", label: "Хэрэглэгчид", priority: 1
  permit_params :email, :password, :password_confirmation, :name, :job, :phone, :photo, :role_id

  index title: "Хэрэглэгчид" do
    column "", class: "project-people" do |obj|
      img src: obj.photo.url(:small), class: "img-circle"
    end
    column :name do |obj|
      link_to obj.name, admin_admin_user_path(obj)
    end
    column :email
    column :phone
    column :role
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
  end

  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  form do |f|
    f.inputs "Хэрэглэгчийн мэдээлэл" do
      div class: :row do 
        f.input :name, wrapper_html: {class: "col-sm-6"}
        f.input :email, wrapper_html: {class: "col-sm-6"}
        f.input :job, wrapper_html: {class: "col-sm-6"}
        f.input :phone, wrapper_html: {class: "col-sm-6"}
        f.input :photo, wrapper_html: {class: "col-sm-6"}
        f.input :role, wrapper_html: {class: "col-sm-6"}
      end
      div class: "hr-line-dashed"
      div class: :row do 
        f.input :password,wrapper_html: {class: "col-sm-6"}
        f.input :password_confirmation, wrapper_html: {class: "col-sm-6"}
      end
    end
    f.actions
  end


  show do 
    div class: :row do 
      div class: "col-lg-4" do 
        div class: "ibox float-e-margins" do 
          div class: "ibox-title" do 
            h5 "Зураг"
          end
          div class: "ibox-content no-padding border-left-right" do 
            img src: admin_user.photo.url(), class: "img-responsive", style: "margin: auto;"
          end
        end
      end
      div class: "col-lg-8" do 
        attributes_table do 
          row :name
          row :email
          row :phone
          row :job
          row :role do 
            status_tag admin_user.role.name, :primary
          end
          row :current_sign_in_at
          row :sign_in_count
          row :created_at
          row :updated_at
        end
      end
    end
    div class: :row do 
      div class: "col-sm-12" do 
        active_admin_comments
      end
    end
  end
end
