ActiveAdmin.register EventCategory do 
	menu parent: "Тохиргоо", label: "Ажлын төрөл"

	permit_params :code, :name, :details
	form do |f|
		f.inputs do 
				f.input :code
				f.input :name
				f.input :details
		end
		f.actions
	end

	index do 
		column :code
		column :name do |obj|
			auto_link obj
		end
		column :details
	end

end