module ActiveAdmin
	module ViewsHelper
		def time_diff from_time, to_time
			distance_of_time_in_words(from_time, to_time)
		end

		def number_format value, is_money = false
				number_with_delimiter value, precision: 2
		end

		def user_photos users
			res = ""
			users.each do |user|
				res << link_to(image_tag(user.photo.url(:small), class: "img-circle m-r-xs"), admin_admin_user_path(user), title: user.name)
			end
			res
		end
	end

	module Views
    module Pages
      class Base < Arbre::HTML::Document
      	def build_activity_content
	      	activities = PublicActivity::Activity.all.order(id: :desc).take(10)
          ul class: "dropdown-menu dropdown-messages" 
        end

        def build_reminder_content
        	div class: "modal", id: "modal-reminder", role: :dialog do
            text_node " "
          end
        end
      end
    end
  end
end