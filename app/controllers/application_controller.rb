class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  include PublicActivity::StoreController

  #alias_method :current_user, :current_admin_user # Could be :current_member or :logged_in_user

  def access_denied(exception)
    redirect_to admin_root_path, :alert => exception.message
  end 

end
