#= require active_admin/base
#= require select2-full

$ ->
  $(".select2").select2({
      tags: true
  })
  $('.full_calendar').fullCalendar({
    header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay,listWeek'
    },
    editable: true,
    droppable: true,
    eventSources: [
      {
      	url: '/admin/calendar/events?format=json', color: '#1ab394'
      },
      {
        url: '/admin/calendar/payments?format=json',
        color: "#ed5565"
      },
      {
        url: '/admin/calendar/tasks?format=json',
        color: "#f8ac59"
      }
    ],
    eventRender: (event ,element) ->
      if event.users.length > 0
        for i in [0..event.users.length]
          if event.users[i] == "all"
            return true
          else
            if $("#user_checker_" + event.users[i] + ":checked").length > 0
              return true
      return false   

    eventDrop: (event, delta, revertFunc)->
      $.ajax 
        url: '/admin/calendar/update_' + event.id
        type: 'GET'
        dataType: 'JSON'
        data:
          start: event.start.format()
          end: event.end.format() if event.end
        success: (data) ->
          toastr.error("Мэдээлэл шинэчилэгдлээ!","Календарь")
     
   
    eventResize: (event, delta, revertFunc)->
      $.ajax 
        url: '/admin/calendar/update_' + event.id
        type: 'GET'
        dataType: 'JSON'
        data:
          start: event.start.format()
          end: event.end.format() if event.end
        success: (data) ->
          toastr.error("Мэдээлэл шинэчилэгдлээ!","Календарь")
    loading: (isLoading, view) ->
      if isLoading
        $(".calendar-updater i").addClass("fa-spin")
      else
        $(".calendar-updater i").removeClass("fa-spin")
   
  })

  $(".calendar-updater").on "click", (event)->
    $(".full_calendar").fullCalendar("rerenderEvents")

  $('.matter-customer').on "change", (event, params)->
    $.ajax
      url: "/admin/customers/"+$(this).val()+"/by_customer" 
      type: "GET"
      dataType: "JSON"
      success: (data) ->
        $('.matter-matter').empty()
        jQuery.each(data, ()->
          newOpt = $("<option value=" + this.value + ">" + this.text + "</option>")
          $(".matter-matter").append(newOpt)
        )
        $(".matter-matter").trigger("chosen:updated")

  $('.customer-changer').on "change", (event,params) ->
    current_url = window.location.href.split("?")[0]
    window.location = current_url + "?customer_type="+$(this).val()

  lineData = {
      labels: [],
      datasets: [
          {
              label: "гэрээ",
              fillColor: "rgba(220,220,220,0.5)",
              strokeColor: "rgba(220,220,220,1)",
              pointColor: "rgba(220,220,220,1)",
              pointStrokeColor: "#fff",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(220,220,220,1)",
              data: []
          },
          {
              label: "харилцагч",
              fillColor: "rgba(26,179,148,0.5)",
              strokeColor: "rgba(26,179,148,0.7)",
              pointColor: "rgba(26,179,148,1)",
              pointStrokeColor: "#fff",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(26,179,148,1)",
              data: []
          }
      ]
  };

  lineOptions = {
      scaleShowGridLines: true,
      scaleGridLineColor: "rgba(0,0,0,.05)",
      scaleGridLineWidth: 1,
      bezierCurve: true,
      bezierCurveTension: 0.4,
      pointDot: true,
      pointDotRadius: 4,
      pointDotStrokeWidth: 1,
      pointHitDetectionRadius: 20,
      datasetStroke: true,
      datasetStrokeWidth: 2,
      datasetFill: true,
      responsive: true,
      multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>"

  };

  if $("#lineChart").length > 0
      $.each $("span.line-chart-customer"), (index, span)->
        lineData.labels[index] = $(span).data("label")
        lineData.datasets[1].data[index] = $(span).data("customer")
        lineData.datasets[0].data[index] = $(span).data("matter")

      line_ctx = document.getElementById("lineChart").getContext("2d")
      lineChart = new Chart(line_ctx).Line(lineData, lineOptions)

  barData = {
      labels: [],
      datasets: [
          {
              label: "Шинэ гэрээ",
              fillColor: "rgba(220,220,220,0.5)",
              strokeColor: "rgba(220,220,220,0.8)",
              highlightFill: "rgba(220,220,220,0.75)",
              highlightStroke: "rgba(220,220,220,1)",
              data: []
          },
          {
              label: "Шинэ харилцагч",
              fillColor: "rgba(26,179,148,0.5)",
              strokeColor: "rgba(26,179,148,0.8)",
              highlightFill: "rgba(26,179,148,0.75)",
              highlightStroke: "rgba(26,179,148,1)",
              data: []
          }
      ]
  }

  barOptions = {
      scaleBeginAtZero: true,
      scaleShowGridLines: true,
      scaleGridLineColor: "rgba(0,0,0,.05)",
      scaleGridLineWidth: 1,
      barShowStroke: true,
      barStrokeWidth: 2,
      barValueSpacing: 5,
      barDatasetSpacing: 1,
      responsive: true,
      multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>"

  }
  if $("#barChart").length > 0
    $.each $("span.bar-chart-customer"), (index, span)->
      barData.labels[index] = $(span).data("label")
      barData.datasets[0].data[index] = $(span).data("matter")
      barData.datasets[1].data[index] = $(span).data("customer")


    ctx = document.getElementById("barChart").getContext("2d");
    myNewChart = new Chart(ctx).Bar(barData, barOptions);

  check_activity()
  check_reminder()
    
  setInterval(check_activity ,60000)



  $(".activity-notification").click((e)->
    if $("ul.dropdown-messages li").length > 0
      if parseInt($("#label-activity").text()) > 0
        $.ajax({
          url: "/admin/public_activity_activities/seen",
          method: 'get',
          data: 
            activity_id: $("ul.dropdown-messages li").first().data("activity")
          success: (data)->
            $("#label-activity").html(0)
        })
  )

check_activity = ()->
  $.ajax({
    url: "/admin/public_activity_activities/get_latests",
    method: "get",
    success: (data)->  
      $("ul.dropdown-messages").html(data)
      $("#label-activity").html($("ul.dropdown-messages li.ibox-heading").length)
  })

check_reminder = () -> 
  $.ajax
    url: "/admin/reminders/check"
    method: "get"
    success: (data) -> 
      $("#modal-reminder").html(data)
      if $("#modal-reminder table tr").length > 0
        $("#modal-reminder").modal()

$(document).on 'click', '.reminder-stopper', (e)->
  that = this
  $.ajax
    url: "/admin/reminders/seen"
    method: 'get'
    data: 
      reminder_id: $(this).data("reminder")
    success: (data)->
      $(that).parents("tr").remove()
